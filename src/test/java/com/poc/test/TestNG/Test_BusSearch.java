package com.poc.test.TestNG;


import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;



import com.poc.genericComponent.SeleniumComponents;
import com.poc.redBus.pageObjectpattern.PageObject_BusSearch;


public class Test_BusSearch extends SeleniumComponents{
	
	public static Logger log= Logger.getLogger(Test_BusSearch.class);
	
	//SoftAssert sAssert= new SoftAssert();
	//end 		sAssert.assertAll();

	
	@Test(dataProvider="dp_getValidBusSearchData", dataProviderClass=com.poc.dataprovider.DataProvider_BusSearch.class,groups={"smoke"})
	public void testvalidSearch(String TC_ID, String Repetition_Order,String Browser,String Search_From, String Search_To,String Search_Joureny, String Expected_Result) throws InterruptedException		
	{
		log.info("Starting Test Case--------"+TC_ID+"--------Repetition Sequence--------" +Repetition_Order);
		
		setWebDriver(setBrowser(Browser));
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---"+Browser+"---Browser launched");
		
		setURL(pRead.propertiesrRetrieve("URL"));
	
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---URL launched");

		PageObject_BusSearch poBSearch= new PageObject_BusSearch(webDriver);
		poBSearch.setFromLocation(Search_From);
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---Entered From Location");

		poBSearch.setToLocation(Search_To);
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---Entered To Location");

//		poBSearch.setJourneyDate(Search_Joureny);
		//poBSearch.searchBusclick(); 


	}
	
	@Test(dataProvider="dp_getInvalidBusSearchData", dataProviderClass=com.poc.dataprovider.DataProvider_BusSearch.class,groups={"smoke"})
	public void testInvalidSearch(String TC_ID, String Repetition_Order,String Browser,String Search_From, String Search_To,String Search_Joureny, String Expected_Result) throws InterruptedException		
	{		

		log.info("Starting Test Case--------"+TC_ID+"--------Repetition Sequence--------" +Repetition_Order);
		
		setWebDriver(setBrowser(Browser));
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---"+Browser+"---Browser launched");
		
		setURL("https://redbus.in");
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---URL launched");

		PageObject_BusSearch poBSearch= new PageObject_BusSearch(webDriver);
		poBSearch.setFromLocation(Search_From);
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---Entered From Location");

		poBSearch.setToLocation(Search_To);
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---Entered To Location");
	}
	
	@Test(dataProvider="dp_getNotYetOpenBusSearchData", dataProviderClass=com.poc.dataprovider.DataProvider_BusSearch.class,groups={"smoke"})
	public void testNotYetOpenSearch(String TC_ID, String Repetition_Order,String Browser,String Search_From, String Search_To,String Search_Joureny, String Expected_Result) throws InterruptedException		
	{		

		log.info("Starting Test Case--------"+TC_ID+"--------Repetition Sequence--------" +Repetition_Order);
		
		setWebDriver(setBrowser(Browser));
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---"+Browser+"---Browser launched");
		
		setURL("https://redbus.in");
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---URL launched");

		PageObject_BusSearch poBSearch= new PageObject_BusSearch(webDriver);
		poBSearch.setFromLocation(Search_From);
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---Entered From Location");

		poBSearch.setToLocation(Search_To);
		log.info("Starting Test Case---"+TC_ID+"---Repetition Sequence---" +Repetition_Order+ "---Entered To Location");
	}
	
		
	
	@AfterMethod
	public void closeActivities()
	{
	}
		
}
	

 