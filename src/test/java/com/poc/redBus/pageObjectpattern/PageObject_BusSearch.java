package com.poc.redBus.pageObjectpattern;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.poc.genericComponent.SeleniumComponents;

public class PageObject_BusSearch extends SeleniumComponents{
	
	//public WebDriver webDriver;
	@FindBy(id="txtSource")
	public WebElement searchFrom;
	
	@FindBy(id="txtDestination")
	public WebElement searchTo; 
	
	@FindBy(id="txtOnwardCalendar")
	public WebElement searchJourneyDate; 
	
	@FindBy(id="txtReturnCalendar")
	public WebElement searchRtnjourneyDate; 
	
	@FindBy(id="searchBtn")
	public WebElement searchBus; 

	@FindBy(id="rbcal_txtOnwardCalendar")
	public WebElement SearchCalendar; 
	
	
	@FindBy(xpath=".//*[@id='rbcal_txtOnwardCalendar']/table[2]/tbody/tr[1]/td[3]/button")
	public WebElement sCalNxtButton; 

	@FindBy(xpath=".//*[@id='onwardTrip']/div[2]/ul/li[1]/div/div[6]/div[1])")
	public WebElement AvailableSeatsCount;
	
	@FindBy(xpath=".//*[@id='onwardTrip']/div[2]/div/div[1]/div[1]/div/span[2] ")
	public WebElement NotYetOpenMsg;
	
	@FindBy(xpath=".//*[@id='onwardTrip']/div[2]/div/div/h7")
	public WebElement NoTicketAva;

	
	public PageObject_BusSearch(WebDriver thewebDriver)
	{
		PageFactory.initElements(thewebDriver,this);
	}
	
	public void setFromLocation(String valuetobeEnter){
		waitforElement(searchFrom);
		elementSendkey(searchFrom, valuetobeEnter);
	}
	
	public void setToLocation(String valuetobeEnter){
		elementSendkey(searchTo,valuetobeEnter);
	}
	
	public void setJourneyDate(String valuetobeEnter) throws InterruptedException{
		elementClick(searchJourneyDate);
		String[] jSelSplit= valuetobeEnter.split("-");
        String jSDate = jSelSplit[0];
        String jSMonth = jSelSplit[1];
        String jSYear = jSelSplit[2];
        while(selectGivenDate( jSDate,jSMonth,jSYear))
        {
        selectNxtMonth();
        }
        Thread.sleep(2000);
	}
	
	public void setRtnJourneyDate(String valuetobeEnter){
		
	}
	
	public void searchBusclick(){
		elementClick(searchBus);
	}
	
	
	public String getNotYetOpenMsg(){
		String elementGetAttribute = elementGetAttribute(NotYetOpenMsg, "text");
		return elementGetAttribute;
	}
	
	public String getAvailableSeatCount(){
		String elementGetAttribute = elementGetAttribute(AvailableSeatsCount, "text");
		return elementGetAttribute;
	}
	
	public String getNotOpenTicketMsg(){
		String elementGetAttribute = elementGetAttribute(NoTicketAva, "text");
		return elementGetAttribute;

	}

	   public boolean selectGivenDate(String jSDate,String jSMonth,String jSYear ) throws InterruptedException
       {
		    int isClicked = 0,isNotClicked=0;
		    System.out.println(isClicked+ isNotClicked);
		   // WebElement jselCalendar = webDriver.findElement(By.id("rbcal_txtOnwardCalendar"));
		    List<WebElement> jseltdList = SearchCalendar.findElements(By.tagName("td"));
		    System.out.println(jseltdList.size());
		    for (WebElement tempjselCalList : jseltdList)
		    {
		           System.out.println(tempjselCalList.getText());
		           if(tempjselCalList.getText().contains(jSMonth))
		                 {
		        	   	System.out.println(tempjselCalList.getText());
		                for (WebElement jseltdListtd : jseltdList) {
						   	   	if(jseltdListtd.getText().equalsIgnoreCase(jSDate))
		                               {
						   	   		System.out.println(jSDate);
		                              System.out.println(jseltdListtd.getText());
		                              elementClick(jseltdListtd);
		                                isClicked =isClicked+1;
		                               break;
		                                		                               }
		                }
		                if(isClicked!=0)break;
		                
		                        }
		           else
		           {
		                 isNotClicked =isNotClicked+1;
		           }

		          
		    }
		    if(isClicked!=0)
		           return true;
		    else
		    	
		           return  false;
		}
       
 
              public void selectNxtMonth() throws InterruptedException
              {
            	  elementClick(sCalNxtButton);
            	  Thread.sleep(1000);
              }
} 	